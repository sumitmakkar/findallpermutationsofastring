#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
        string str;
        string compressedString;
    
        vector<int> populateCountVector()
        {
            vector<int> count;
            vector<int> alpha;
            int         len   = (int)str.length();
            for(int i = 0 ; i < 26 ; i++)
            {
                alpha.push_back(0);
            }
            for(int i = 0 ; i < len ; i++)
            {
                alpha[str[i] - 'A']++;
            }
            for(int i = 0 ; i < 26 ; i++)
            {
                if(alpha[i])
                {
                    count.push_back(alpha[i]);
                    compressedString += i + 'A';
                }
            }
            return count;
        }
    
        void permutationUtil(string str , vector<int> count , string result , int level , vector<string> *resultList)
        {
            if(level == (int)str.length())
            {
                resultList->push_back(result);
                return;
            }
            for(int i = 0 ; i < (int)str.length() ; i++)
            {
                if(count[i] == 0)
                {
                    continue;
                }
                result[level] = compressedString[i];
                count[i]--;
                permutationUtil(str , count , result , level + 1 , resultList);
                count[i]++;
            }
        }
    
        void displayList(vector<string> resultList)
        {
            int len = (int)resultList.size();
            for(int i = 0 ; i < len ; i++)
            {
                cout<<resultList[i]<<endl;
            }
        }
    
    public:
        Engine(string s)
        {
            str = s;
        }
    
        void permuteAll()
        {
            vector<int> count = populateCountVector();
            vector<string> resultList;
            string result = "    ";
            permutationUtil(str , count , result , 0 , &resultList);
            displayList(resultList);
        }
};

int main(int argc, const char * argv[])
{
    string str = "AABC";
    Engine e   = Engine(str);
    e.permuteAll();
    return 0;
}
